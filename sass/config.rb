# プロジェクトルート
http_path = "/"
# CSSファイル出力ディレクトリ
css_dir = "css"
# Sassディレクトリ
sass_dir = "sass"
# 画像ディレクトリ
images_dir = "img"
# jsディレクトリ
javascripts_dir = "js"
# 出力するCSSのスタイル（:compressed = 圧縮版の出力）
output_style = :compressed
#相対パス（これを入れないと絶対パスになり環境が変わった時にCSSが使えない）
relative_assets = true
